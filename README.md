### What is this repository for? ###

* Automation framework in c# based on an alternative pattern than the usual PageObject
* Version 1.0

The raw architecture would look something like this:

![Capture.PNG](https://bitbucket.org/repo/dBXXd7/images/1248239328-Capture.PNG)

ControlBase contains general methods needed for all webelements.

The abstract layer would implement every control's specific action (like dropdown has a select value and button does not). Every control class will contain full method implementation, including asserts, exception catching and logging.

The static layer will just instantiate the control and act as a proxy for the webelement, so we could implement the actual testcases in a easier way, as in the raw example below.

The locators would be later stored in properties files.

![Capture.PNG](https://bitbucket.org/repo/dBXXd7/images/3808773081-Capture.PNG)