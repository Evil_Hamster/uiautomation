﻿using Automation.BaseFramework.BaseLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.BaseFramework.StaticLayer
{
    public class Checkbox : CheckboxBase
    {
        /// <summary>
        /// Selects all values in a dropdown
        /// </summary>
        /// <param name="CheckboxLocator"></param>
        public static new void SelectAllOptions (string CheckboxLocator)
        {
            CheckboxBase cb = new CheckboxBase();
            cb.SelectAllOptions(CheckboxLocator);
        }
    }
}
