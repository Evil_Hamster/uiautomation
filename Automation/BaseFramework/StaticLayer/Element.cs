﻿using Automation.BaseFramework.BaseLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.StaticLayer
{
    public class Element : ControlBase
    {

        /// <summary>
        /// Clicks on a iWebElement.
        /// </summary>
        /// <param name="ElementLocator">String on how to find the element</param>
        /// <param name="LocatorType">It can be xpath, id, etc</param>
        public static void ClickElement(string ElementLocator)
        {
            ControlBase control = new ControlBase();
            control.ClickElement(ElementLocator);
        }

        /// <summary>
        /// Sends text to a IWebElement
        /// </summary>
        /// <param name="ElementLocator">Locator of the element</param>
        /// <param name="LocatorType"> Type of the element. It can be: xpath, id</param>
        /// <param name="Text">Text to be send to the element</param>
        public static void SendText(string ElementLocator, string Text)
        {
            ControlBase control = new ControlBase();
            control.SendText(ElementLocator, Text);
        }
    }
}
