﻿using Automation.BaseFramework.BaseLayer.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.BaseFramework.StaticLayer
{
    public class Dropdown : DropdownBase
    {
        /// <summary>
        /// Static method for selecting a value in a dropdown
        /// Can be easily called preceded by class name.
        /// Example: Dropdown.SelectValueByName("xpath", "value1");
        /// </summary>
        /// <param name="DropdownLocator">xpath of the dropdown</param>
        /// <param name="DropdownValue">value to select</param>
        public static new void SelectValueByName(string DropdownLocator, string DropdownValue)
        {
            DropdownBase db = new DropdownBase();
            db.SelectValueByName(DropdownLocator, DropdownValue);
        }

        /// <summary>
        /// Selects a value in the specified dropdown by index
        /// </summary>
        /// <param name="DropdownLocator">Locator of the element</param>
        /// <param name="index">Index of the element to be selected</param>
        public static new void SelectValueByIndex(string DropdownLocator, int index)
        {
            DropdownBase db = new DropdownBase();
            db.SelectValueByIndex(DropdownLocator, index);
        }

        /// <summary>
        /// Gets the selected value in a dropdown
        /// </summary>
        /// <param name="DropdownLocator">Locator of the element.</param>
        /// <returns>Returns the selected element as a string</returns>
        public static new string GetSelectedValue(string DropdownLocator)
        {
            DropdownBase db = new DropdownBase();
            return db.GetSelectedValue(DropdownLocator);
        }
    }
}
