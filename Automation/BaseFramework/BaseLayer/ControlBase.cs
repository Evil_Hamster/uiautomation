﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.BaseFramework.BaseLayer
{
    public class ControlBase
    {
        BrowserManager browser = new BrowserManager();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ElementLocator"></param>
        public void ClickElement(string ElementLocator)
        {
            try
            {
                IWebElement elem = BrowserManager.findElement(ElementLocator);
                elem.Click();
                Console.WriteLine("Succesfully clicked on the element + " + ElementLocator);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception occured: " + e.ToString());
                Assert.Fail("Failed to click on the element: " + ElementLocator);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ElementLocator"></param>
        /// <param name=""></param>
        /// <param name="TextToBeSent"></param>
        public void SendText(string ElementLocator, string TextToBeSent)
        {
            try
            {
                IWebElement elem = BrowserManager.findElement(ElementLocator);
                elem.SendKeys(TextToBeSent);
                Console.WriteLine("Successfully sent " + TextToBeSent + " text to the element: " + ElementLocator);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail("Failed to send text to the element" + ElementLocator);

            }
        }
    }
}
