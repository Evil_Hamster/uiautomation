﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using Automation.Logger;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;

namespace Automation.BaseFramework.BaseLayer
{
    public class BrowserManager
    {
        private static IWebDriver browser;

        /// <summary>
        /// The  constructor of the class. Maximizes the web browser before doing any action on it
        /// </summary>
        public BrowserManager()
        {
            //implicit constructor;
            browser.Manage().Window.Maximize();

        }


        /// <summary>
        /// Explicitly waits for an element to become displayed.
        /// </summary>
        /// <param name="Element">The xpath of the element</param>
        /// <param name="Timeout">The timespan of the element to become visible</param>
        public static void WaitForElementToBeDisplayed(string Element, int Timeout)
        {
            try
            {
                Console.WriteLine("Waiting " + Timeout.ToString() + " for the " + Element + "to become displayed");
                TimeSpan t = new TimeSpan(0, 0, 0, Timeout);
                WebDriverWait wait = new WebDriverWait(browser, t);
                wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(Element)));

                Console.WriteLine("Element " + Element + " became visible.");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail("Element " + Element + "failed to become displayed.");
            }
        }

        /// <summary>
        /// Refreshes the web browser.
        /// </summary>
        public static void Refresh()
        {
            try
            {
                browser.Navigate().Refresh();
                Console.WriteLine("Successfully refreshed the page.");
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
                Console.Write("Failed to refresh the page" + e.Message);
            }
        }

        /// <summary>
        /// An explicit constructor where you can specify the browser instance and starting page
        /// </summary>
        /// <param name="driver">drivermanager.exe instance</param>
        /// <param name="startingPage">Staring page of the application</param>
        public BrowserManager(string driver, string startingPage)
        {
            if (driver.Contains("chrome"))
            {
                var options = new ChromeOptions();
                options.AddArgument("start-maximized");
                browser = new ChromeDriver(@"D:\Selenium3.0", options);
                Console.WriteLine("Started chromedriver instance.");
            }

            if (driver.Contains("firefox") || driver.Contains("Firefox"))
            {
                Console.WriteLine("Started firefoxdriver instance.");
                browser = new FirefoxDriver();
            }
        }

        /// <summary>
        /// Navigates to a page having a string specified as an argument
        /// </summary>
        /// <param name="pageToNavigate">Page to navigate to</param>
        public static void Navigate(string pageToNavigate)
        {
            try
            {
                browser.Navigate().GoToUrl(pageToNavigate);
                Console.WriteLine("Navigated to: ." + pageToNavigate);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }


        }

        /// <summary>
        /// Searches for all the elements on a web page having the same locator
        /// Returns a list of the elements
        /// </summary>
        /// <param name="ElementLocator">Locator of the element</param>
        /// <param name="LocatorType">Type of the locator.</param>
        /// <returns>Returns a list of webelements</returns>
        public static LinkedList<IWebElement> findElements(string ElementLocator, string LocatorType)
        {
            LinkedList<IWebElement> list;

            if (LocatorType.Equals("xpath"))
            {
                try
                {
                    IReadOnlyCollection<IWebElement> rawList = browser.FindElements(By.XPath(ElementLocator));
                    list = new LinkedList<IWebElement>(rawList);
                    Console.WriteLine("Successfully found " + list.Count + " elements with the locator" + ElementLocator);
                }
                catch (Exception e)
                {
                    list = null;
                    Assert.Fail("Failed to find elements " + ElementLocator);
                }
            }
            else
            {
                list = new LinkedList<IWebElement>();
            }

            return list;
        }

        /// <summary>
        /// Searches for a single element within a web page
        /// </summary>
        /// <param name="ElementLocator">Locator of the element</param>
        /// <returns></returns>
        public static IWebElement findElement(string ElementLocator)
        {
            IWebElement element;
            try
            {
                element = browser.FindElement(By.XPath(ElementLocator));
                Console.WriteLine("Successfully found the element" + ElementLocator);
            }
            catch (Exception e)
            {
                element = null;
                Assert.Fail("Failed to find element " + ElementLocator);
                Console.WriteLine("Exception reached" + e.Message);
            }

            return element;
        }


        /// <summary>
        /// Closes the browser and destroys the browser object.
        /// </summary>
        public static void QuitBrowser()
        {
            Console.WriteLine("Closed the driver instance.");
            //LoggerHelper.CloseLogger();
            browser.Quit();
        }
    }
}
