﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.BaseFramework.BaseLayer.Controls
{
    public class DropdownBase : ControlBase
    {

        /// <summary>
        /// Selects a value in a dropdown based on a string value
        /// </summary>
        /// <param name="DropdownLocator">xpath of the dropdown</param>
        /// <param name="Value">Value in the dropdown that's going to be selected</param>
        public void SelectValueByName(string DropdownLocator, string Value)
        {
            Console.WriteLine("Selecting " + Value + " in " + DropdownLocator);
            IWebElement dropdown = BrowserManager.findElement(DropdownLocator);

            try
            {
                SelectElement selector = new SelectElement(dropdown);
                selector.SelectByText(Value);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail("Failed to select value in dropdownw.");
            }
        }


        /// <summary>
        /// Selects a value in a dropdown by an index specified as parameter
        /// </summary>
        /// <param name="DropdownLocator">The xpath of the </param>
        /// <param name="index"></param>
        public void SelectValueByIndex(string DropdownLocator, int index)
        {
            Console.WriteLine("Selecting index no: " + index + " in " + DropdownLocator);
            IWebElement elem = BrowserManager.findElement(DropdownLocator);

            try
            {
                SelectElement selector = new SelectElement(elem);
                selector.SelectByIndex(index);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Returns the selected value in the dropdown
        /// </summary>
        /// <param name="DropdownLocator"></param>
        /// <returns></returns>
        public string GetSelectedValue(string DropdownLocator)
        {
            Console.WriteLine("Getting selected value ofthe dropdown:" + DropdownLocator);
            IWebElement elem = BrowserManager.findElement(DropdownLocator);
            string returnElement = "";
            try
            {
                SelectElement selector = new SelectElement(elem);
                returnElement = selector.SelectedOption.Text;
                Console.WriteLine("Selected value is " + returnElement);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail("Could not get selected value.");
            }

            return returnElement;
        }

    }
}
