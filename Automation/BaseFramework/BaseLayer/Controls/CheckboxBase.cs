﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.BaseFramework.BaseLayer
{
    public class CheckboxBase : ControlBase
    {
        public void SelectAllOptions(string ControlLocator)
        {
            Console.WriteLine("Selecting all the values in" + ControlLocator);

            LinkedList<IWebElement> checkboxes = BrowserManager.findElements(ControlLocator, "xpath");

            try
            {
                SelectElement selector = new SelectElement(checkboxes.First());
                for (int i = 0; i < selector.AllSelectedOptions.Count; i++)
                {
                    selector.SelectByIndex(i);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail("Could not select all the values in the " + ControlLocator + " checkbox.");
            }
        }
    }
}
